const express = require("express");
const router = express.Router();
// llamamos a nuestra conexion de base de datos
const conexion = require("./database/db");
// declaramos el modulo para encriptar la contraseña
const bcryptjs = require("bcryptjs");

//  **********RUTAS CONFIGURADAS***************

// ruta raiz del sitio web
router.get("/", (req, res) => {
  res.render("index", {
    login: false,
    name: "INICIO",
    entry: false,
  });

  // if (req.session.loggedin) {
  //   res.render("index", {
  //     login: true,
  //     name: req.session.name,
  //     entry: true,
  //   });
  // } else {
  //   if (req.session.loginusuario) {
  //     res.render("index", {
  //       login: true,
  //       name: req.session.name,
  //       entry: false,
  //     });
  //   } else {
  //     res.render("index", {
  //       login: false,
  //       name: "INICIO",
  //       entry: false,
  //     });
  //   }
  // }
});

//  **********SECCION DE USUARIOS***************

// ruta del login de los usuarios
router.get("/login", (req, res) => {
  res.render("login", {
    login: false,
    name: "INICIA SESIÓN",
    entry: false,
  });

  //   res.redirect("/");
  // } else {
  //   if (req.session.loginusuario) {
  //     res.redirect("/usuario");
  //   } else {
  //     res.render("login", {
  //       login: false,
  //       name: "INICIA SESIÓN",
  //       entry: false,
  //     });
  //   }
  // }
});

// peticion de acceso a login del cliente
var datausuario = [];
router.post("/login-auth", async (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  if (email && password) {
    conexion.query(
      "SELECT * FROM usuario WHERE US_EMAIL = '" + email + "'",
      async (error, results) => {
        if (error) throw error;
        datausuario = results;
        if (datausuario.length == 0) {
          res.render("login", {
            alert: true,
            alertTitle: "Error",
            alertMessage:
              "!No tienes una cuenta, solicita el servicio para poder acceder al sistema!",
            alertIcon: "error",
            showConfirmButton: true,
            timer: false,
            ruta: "login",
            login: false,
            name: "INICIA SESIÓN",
            entry: false,
          });
        } else {
          if (bcryptjs.compareSync(password, datausuario[0].US_CONTRASENA)) {
            // req.session.loginusuario = true;
            req.session.name = datausuario[0].US_NOMBRE;
            req.session.identificacion = datausuario[0].US_ID;
            res.render("login", {
              alert: true,
              alertTitle: "Conexión Exitosa",
              alertMessage: "!LOGIN CORRECTO!",
              alertIcon: "success",
              showConfirmButton: false,
              timer: 1500,
              ruta: "usuario",
              login: true,
              name: req.session.name,
              entry: false,
            });
          } else {
            res.render("login", {
              alert: true,
              alertTitle: "Error",
              alertMessage: "!Contraseña Incorrecta!",
              alertIcon: "error",
              showConfirmButton: true,
              timer: false,
              ruta: "login",
              login: false,
              name: "INICIA SESIÓN",
              entry: false,
            });
          }
        }
      }
    );
  } else {
    res.render("login", {
      alert: true,
      alertTitle: "Advertencia",
      alertMessage: "!Por favor ingrese un usuario y/o contraseña!",
      alertIcon: "warning",
      showConfirmButton: true,
      timer: false,
      ruta: "login",
      login: false,
      name: "INICIA SESIÓN",
      entry: false,
    });
  }
});

// ruta del CRUD de planillas
router.get("/usuario", (req, res) => {
  const id = req.session.identificacion;
  conexion.query(
    "select * from planilla where US_ID =?",
    [id],
    (error, results) => {
      if (error) {
        throw error;
      } else {
        res.render("usuario", {
          login: true,
          name: req.session.name,
          results: results,
          entry: false,
        });
      }
    }
  );

  // if (req.session.loginusuario) {
  //   const id = req.session.identificacion;
  //   conexion.query(
  //     "select * from planilla where US_ID =?",
  //     [id],
  //     (error, results) => {
  //       if (error) {
  //         throw error;
  //       } else {
  //         res.render("usuario", {
  //           login: true,
  //           name: req.session.name,
  //           results: results,
  //           entry: false,
  //         });
  //       }
  //     }
  //   );
  // } else {
  //   res.redirect("/");
  // }
});

//  **********SECCION DE ADMINISTRACION***************

// ruta para el login del admin
router.get("/admin", (req, res) => {
  res.render("admin_login", {
    login: false,
    name: "INICIA SESIÓN COMO ADMINISTRADOR",
    entry: true,
  });

  // if (req.session.loginusuario) {
  //   res.redirect("/");
  // } else {
  //   if (req.session.loggedin) {
  //     res.redirect("/administracion");
  //   } else {
  //     res.render("admin_login", {
  //       login: false,
  //       name: "INICIA SESIÓN COMO ADMINISTRADOR",
  //       entry: true,
  //     });
  //   }
  // }
});

// ruta para la auntenticacion del admin
var dataadmin = [];
router.post("/admin-auth", async (req, res) => {
  const user = req.body.user;
  const pass = req.body.pass;
  if (user && pass) {
    conexion.query(
      "SELECT * FROM administrador WHERE ADM_EMAIL = '" + user + "'",
      async (error, results) => {
        if (error) throw error;
        dataadmin = results;
        if (dataadmin.length == 0) {
          res.render("admin_login", {
            alert: true,
            alertTitle: "Error",
            alertMessage: "!No eres Administrador!",
            alertIcon: "error",
            showConfirmButton: true,
            timer: false,
            ruta: "admin",
            login: false,
            name: "INICIA SESIÓN COMO ADMINISTRADOR",
            entry: true,
          });
        } else {
          if (pass == dataadmin[0].ADM_CONTRASENA) {
            // req.session.loggedin = true;
            req.session.name = dataadmin[0].ADM_NOMBRE;
            req.session.codigo = dataadmin[0].ADM_ID;
            res.render("admin_login", {
              alert: true,
              alertTitle: "Conexión Exitosa",
              alertMessage: "!LOGIN CORRECTO!",
              alertIcon: "success",
              showConfirmButton: false,
              timer: 1500,
              ruta: "administracion",
              login: true,
              name: req.session.name,
              entry: true,
            });
          } else {
            res.render("admin_login", {
              alert: true,
              alertTitle: "Error",
              alertMessage: "!Contraseña Incorrecta!",
              alertIcon: "error",
              showConfirmButton: true,
              timer: false,
              ruta: "admin",
              login: false,
              name: "INICIA SESIÓN COMO ADMINISTRADOR",
              entry: true,
            });
          }
        }
      }
    );
  } else {
    res.render("admin_login", {
      alert: true,
      alertTitle: "Advertencia",
      alertMessage: "!Por favor ingrese un usuario y/o contraseña!",
      alertIcon: "warning",
      showConfirmButton: true,
      timer: false,
      ruta: "admin",
      login: false,
      name: "INICIA SESIÓN COMO ADMINISTRADOR",
      entry: true,
    });
  }
});

// ruta para cerrar la sesion
router.get("/logout", (req, res) => {
  req.session.destroy(() => {
    res.redirect("/");
  });
});

// ruta de la pagina de administracion
router.get("/administracion", (req, res) => {
  res.render("administracion", {
    login: true,
    name: req.session.name,
    entry: true,
  });
  // if (req.session.loggedin) {
  //   res.render("administracion", {
  //     login: true,
  //     name: req.session.name,
  //     entry: true,
  //   });
  // } else {
  //   res.redirect("/");
  // }
});

// ruta del CRUD de productos
router.get("/admin-products", (req, res) => {
  conexion.query("select * from producto", (error, results) => {
    if (error) {
      throw error;
    } else {
      res.render("crud_products", {
        login: true,
        name: req.session.name,
        results: results,
        entry: true,
      });
    }
  });

  // if (req.session.loggedin) {
  //   conexion.query("select * from producto", (error, results) => {
  //     if (error) {
  //       throw error;
  //     } else {
  //       res.render("crud_products", {
  //         login: true,
  //         name: req.session.name,
  //         results: results,
  //         entry: true,
  //       });
  //     }
  //   });
  // } else {
  //   res.redirect("/");
  // }
});

// Ruta para crear registros de productos
router.get("/create", (req, res) => {
  res.render("create", {
    login: true,
    name: req.session.name,
    codigo: req.session.codigo,
    entry: true,
  });
  // if (req.session.loggedin) {
  //   res.render("create", {
  //     login: true,
  //     name: req.session.name,
  //     codigo: req.session.codigo,
  //     entry: true,
  //   });
  // } else {
  //   res.redirect("/");
  // }
});

// ruta para guardar un producto
router.post("/save", (req, res) => {
  const admin = req.body.admin;
  const codigo = req.body.codigo;
  const serie = req.body.serie;
  const voltaje = req.body.voltaje;
  const ubicacion = req.body.ubicacion;
  const tipo = req.body.tipo;
  const tamano = req.body.tamano;
  const peso = req.body.peso;
  const color = req.body.color;

  conexion.query(
    "insert into producto  SET ?",
    {
      ADM_ID: admin,
      PRO_CODIGO: codigo,
      PRO_SERIE: serie,
      PRO_VOLTAJE: voltaje,
      PRO_UBICACION: ubicacion,
      PRO_TIPO: tipo,
      PRO_TAMANO: tamano,
      PRO_PESO: peso,
      PRO_COLOR: color,
    },
    (error, results) => {
      if (error) {
        console.log(error);
      } else {
        res.redirect("/create");
      }
    }
  );
});

// Ruta para editar registros de productos
router.get("/edit/:id", (req, res) => {
  const id = req.params.id;
  conexion.query(
    "select * from producto where PRO_ID=?",
    [id],
    (error, results) => {
      if (error) {
        throw error;
      } else {
        res.render("edit", {
          login: true,
          name: req.session.name,
          product: results[0],
          codigo: req.session.codigo,
          entry: true,
        });
      }
    }
  );
  // if (req.session.loggedin) {
  //   const id = req.params.id;
  //   conexion.query(
  //     "select * from producto where PRO_ID=?",
  //     [id],
  //     (error, results) => {
  //       if (error) {
  //         throw error;
  //       } else {
  //         res.render("edit", {
  //           login: true,
  //           name: req.session.name,
  //           product: results[0],
  //           codigo: req.session.codigo,
  //           entry: true,
  //         });
  //       }
  //     }
  //   );
  // } else {
  //   res.redirect("/");
  // }
});

// ruta para actualizar un producto
router.post("/update", (req, res) => {
  const id = req.body.id;
  const admin = req.body.admin;
  const codigo = req.body.codigo;
  const serie = req.body.serie;
  const voltaje = req.body.voltaje;
  const ubicacion = req.body.ubicacion;
  const tipo = req.body.tipo;
  const tamano = req.body.tamano;
  const peso = req.body.peso;
  const color = req.body.color;

  conexion.query(
    "update producto SET ? where PRO_ID = ?",
    [
      {
        ADM_ID: admin,
        PRO_CODIGO: codigo,
        PRO_SERIE: serie,
        PRO_VOLTAJE: voltaje,
        PRO_UBICACION: ubicacion,
        PRO_TIPO: tipo,
        PRO_TAMANO: tamano,
        PRO_PESO: peso,
        PRO_COLOR: color,
      },
      id,
    ],
    (error, results) => {
      if (error) {
        console.log(error);
      } else {
        res.redirect("/admin-products");
      }
    }
  );
});

// Ruta para eliminar registros de productos
router.get("/delete/:id", (req, res) => {
  const id = req.params.id;
  conexion.query(
    "delete from producto where PRO_ID = ?",
    [id],
    (error, results) => {
      if (error) {
        throw error;
      } else {
        res.redirect("/admin-products");
      }
    }
  );
  // if (req.session.loggedin) {
  //   const id = req.params.id;
  //   conexion.query(
  //     "delete from producto where PRO_ID = ?",
  //     [id],
  //     (error, results) => {
  //       if (error) {
  //         throw error;
  //       } else {
  //         res.redirect("/admin-products");
  //       }
  //     }
  //   );
  // } else {
  //   res.redirect("/");
  // }
});

// ruta del CRUD de clientes
router.get("/admin-clientes", (req, res) => {
  conexion.query("select * from usuario", (error, results) => {
    if (error) {
      throw error;
    } else {
      res.render("crud_clientes", {
        login: true,
        name: req.session.name,
        results: results,
        entry: true,
      });
    }
  });

  // if (req.session.loggedin) {
  //   conexion.query("select * from usuario", (error, results) => {
  //     if (error) {
  //       throw error;
  //     } else {
  //       res.render("crud_clientes", {
  //         login: true,
  //         name: req.session.name,
  //         results: results,
  //         entry: true,
  //       });
  //     }
  //   });
  // } else {
  //   res.redirect("/");
  // }
});

// Ruta para crear registros de clientes
router.get("/create-cliente", (req, res) => {
  res.render("create_cliente", {
    login: true,
    name: req.session.name,
    entry: true,
  });

  // if (req.session.loggedin) {
  //   res.render("create_cliente", {
  //     login: true,
  //     name: req.session.name,
  //     entry: true,
  //   });
  // } else {
  //   res.redirect("/");
  // }
});

// ruta para guardar un cliente
router.post("/save-cliente", (req, res) => {
  const apellido = req.body.apellido;
  const nombre = req.body.nombre;
  const email = req.body.email;
  const pass = req.body.pass;
  const direccion = req.body.direccion;
  const tipo = req.body.tipo;
  const cedula = req.body.cedula;
  const celular = req.body.celular;
  const genero = req.body.genero;
  var passHash = bcryptjs.hashSync(pass, 8);

  conexion.query(
    "insert into usuario  SET ?",
    {
      US_APELLIDO: apellido,
      US_NOMBRE: nombre,
      US_EMAIL: email,
      US_CONTRASENA: passHash,
      US_DIRECCION: direccion,
      US_TIPO_PERSONA: tipo,
      US_CEDULA: cedula,
      US_CELULAR: celular,
      US_GENERO: genero,
    },
    (error, results) => {
      if (error) {
        console.log(error);
      } else {
        res.redirect("/create-cliente");
      }
    }
  );
});

// Ruta para editar registros de productos
router.get("/edit-cliente/:id", (req, res) => {
  const id = req.params.id;
  conexion.query(
    "select * from usuario where US_ID=?",
    [id],
    (error, results) => {
      if (error) {
        throw error;
      } else {
        res.render("edit_cliente", {
          login: true,
          name: req.session.name,
          product: results[0],
          entry: true,
        });
      }
    }
  );

  // if (req.session.loggedin) {
  //   const id = req.params.id;
  //   conexion.query(
  //     "select * from usuario where US_ID=?",
  //     [id],
  //     (error, results) => {
  //       if (error) {
  //         throw error;
  //       } else {
  //         res.render("edit_cliente", {
  //           login: true,
  //           name: req.session.name,
  //           product: results[0],
  //           entry: true,
  //         });
  //       }
  //     }
  //   );
  // } else {
  //   res.redirect("/");
  // }
});

// ruta para actualizar un cliente
router.post("/update-cliente", (req, res) => {
  const id = req.body.id;
  const apellido = req.body.apellido;
  const nombre = req.body.nombre;
  const email = req.body.email;
  const direccion = req.body.direccion;
  const tipo = req.body.tipo;
  const cedula = req.body.cedula;
  const celular = req.body.celular;
  const genero = req.body.genero;

  conexion.query(
    "update usuario SET ? where US_ID = ?",
    [
      {
        US_APELLIDO: apellido,
        US_NOMBRE: nombre,
        US_EMAIL: email,
        US_DIRECCION: direccion,
        US_TIPO_PERSONA: tipo,
        US_CEDULA: cedula,
        US_CELULAR: celular,
        US_GENERO: genero,
      },
      id,
    ],
    (error, results) => {
      if (error) {
        console.log(error);
      } else {
        res.redirect("/admin-clientes");
      }
    }
  );
});

// Ruta para eliminar registros de clientes
router.get("/delete-cliente/:id", (req, res) => {
  const id = req.params.id;
  conexion.query(
    "delete from usuario where US_ID = ?",
    [id],
    (error, results) => {
      if (error) {
        throw error;
      } else {
        res.redirect("/admin-clientes");
      }
    }
  );

  // if (req.session.loggedin) {
  //   const id = req.params.id;
  //   conexion.query(
  //     "delete from usuario where US_ID = ?",
  //     [id],
  //     (error, results) => {
  //       if (error) {
  //         throw error;
  //       } else {
  //         res.redirect("/admin-clientes");
  //       }
  //     }
  //   );
  // } else {
  //   res.redirect("/");
  // }
});

module.exports = router;
