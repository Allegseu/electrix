// declaracion de los inputs como variables
const inputs = document.querySelectorAll("form input"),
  // declaracion del contenedor (p) para los mensajes de alerta
  adver = document.getElementById("warning");

// declaracion de las expresiones regulares para la validacion
const expresionesRegulares = {
  numeros: /([0-9])/,
  text: /([a-zA-Z])/,
  caracteres: /[^a-zA-Z\d\s]/,
  correo:
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
  espacios: /\s/g,
};
//   codigo que valida el contenido de los inputs
inputs.forEach((input) => {
  input.addEventListener("keyup", (e) => {
    let valueInput = e.target.value;
    let style = e.target.style;

    switch (e.target.id) {
      case "email":
        input.value = valueInput.replace(expresionesRegulares.espacios, "");
        if (expresionesRegulares.correo.test(valueInput)) {
          style.border = " 2px solid #008f39";
          message("Formato de correo electrónico correcto");
        } else {
          style.border = "2px solid #ce1212";
          message("Formato de correo electrónico incorrecto");
        }
        break;

      case "password":
        input.value = valueInput.replace(expresionesRegulares.espacios, "");
        if (valueInput.length < 8) {
          message("La contraseña no puede ser menor a 8 carácteres");
          style.border = "2px solid #ce1212";
        } else {
          message("Longitud de contraseña correcta");
          style.border = "2px solid #008f39";
        }
        break;
    }
  });
});

// funcion que muestra un mensaje de alerta
function message(val) {
  adver.innerHTML = val;
}
