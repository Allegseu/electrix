$(document).ready(function () {
  $("#table").DataTable({
    pageLength: 5,
    lengthMenu: [
      [5, 10, 20, -1],
      [5, 10, 20, "Todos"],
    ],
    language: {
      lengthMenu: "Mostrar _MENU_ registros",
      zeroRecords: "No se encontraron resultados",
      info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
      infoFiltered: "(filtrado de un total de _MAX_ registros)",
      sSearch: "Buscar:",
      oPaginate: {
        sFirst: "Primero",
        sLast: "Último",
        sNext: "Siguiente",
        sPrevious: "Anterior",
      },
      sProcessing: "Procesando...",
    },
  });
});

function confirmar(id) {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-danger mx-2",
      cancelButton: "btn btn-info mx-2",
    },
    buttonsStyling: false,
  });

  swalWithBootstrapButtons
    .fire({
      title: "Esta seguro de Eliminar el registro " + id + " ?",
      text: "No podras revertir esta operación!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Sí, Eliminar!",
      cancelButtonText: "Cancelar",
      reverseButtons: true,
    })
    .then((result) => {
      if (result.isConfirmed) {
        swalWithBootstrapButtons.fire(
          "Eliminado!",
          "El registro ha sido eliminado de la base de datos",
          "success"
        );

        const myCallback = () => (window.location = "/delete/" + id);
        setTimeout(myCallback, 1500);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        swalWithBootstrapButtons.fire(
          "Cancelado",
          "La operación ha sido cancelada",
          "info"
        );
      }
    });
}


// metodo de eliminacion para clientes
function confirmar2(id) {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-danger mx-2",
      cancelButton: "btn btn-info mx-2",
    },
    buttonsStyling: false,
  });

  swalWithBootstrapButtons
    .fire({
      title: "Esta seguro de Eliminar el registro " + id + " ?",
      text: "No podras revertir esta operación!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Sí, Eliminar!",
      cancelButtonText: "Cancelar",
      reverseButtons: true,
    })
    .then((result) => {
      if (result.isConfirmed) {
        swalWithBootstrapButtons.fire(
          "Eliminado!",
          "El registro ha sido eliminado de la base de datos",
          "success"
        );

        const myCallback = () => (window.location = "/delete-cliente/" + id);
        setTimeout(myCallback, 1500);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        swalWithBootstrapButtons.fire(
          "Cancelado",
          "La operación ha sido cancelada",
          "info"
        );
      }
    });
}