const mysql = require("mysql");

// creamos la conexion con la base de datos local
// const conexion = mysql.createConnection({
//   host: "localhost",
//   user: "root",
//   password: "",
//   database: "electrixbd",
// });

// conexion a base de datos online en db4free.net
const conexion = mysql.createConnection({
  host: "db4free.net",
  user: "mikasa",
  password: "mikasa123@",
  database: "espinales",
});

// conectamos con la base de datos
conexion.connect((error) => {
  if (error) {
    console.log("El error de conexion es: " + error);
    return;
  } else {
    console.log("Conexion a la Base de Datos Exitosa");
  }
});

module.exports = conexion;
